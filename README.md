
# Step 1: Installing Milvus
## 1. Install Milvus Standalone with Docker
Start Milvus:
```angular2html
curl -0 https://raw.githubusercontent.com/milvus-io/milvus/master/scripts/standalone_embed.sh
bash standalone_embed.sh start
```
The result is be like:
```angular2html
(base) supergeorge@wangzhengqideMacBook-Pro MilvusProject % curl -O https://raw.githubusercontent.com/milvus-io/milvus/master/scripts/standalone_embed.sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3318  100  3318    0     0  46445      0 --:--:-- --:--:-- --:--:-- 50272
(base) supergeorge@wangzhengqideMacBook-Pro MilvusProject % bash standalone_embed.sh start
Wait for Milvus Starting...
Start successfully.
```


# Step 2: Setting Up Milvus
## 1. Connect to Milvus
Create hello_milvus.py to a demo.
```angular2html
import time

import numpy as np
from pymilvus import (
    connections,
    utility,
    FieldSchema, CollectionSchema, DataType,
    Collection,
)

fmt = "\n=== {:30} ===\n"
search_latency_fmt = "search latency = {:.4f}s"
num_entities, dim = 3000, 8
```

# Step 3: Ingesting Data into Milvus
## 1. Connects to a server
```angular2html
connections.connect("default", host="localhost", port="19530")
```

## 2. Creates a collection
```angular2html
fields = [
    FieldSchema(name="pk", dtype=DataType.INT64, is_primary=True, auto_id=False),
    FieldSchema(name="random", dtype=DataType.DOUBLE),
    FieldSchema(name="embeddings", dtype=DataType.FLOAT_VECTOR, dim=8)
]
schema = CollectionSchema(fields, "hello_milvus is the simplest demo to introduce the APIs")
hello_milvus = Collection("hello_milvus", schema)
```

## 3. Builds indexes on the entities:
```angular2html
index = {
    "index_type": "IVF_FLAT",
    "metric_type": "L2",
    "params": {"nlist": 128},
}
hello_milvus.create_index("embeddings", index)
```
## 4. Loads the collection to memory and performs a vector similarity search:
```angular2html
hello_milvus.load()
vectors_to_search = entities[-1][-2:]
search_params = {
    "metric_type": "L2",
    "params": {"nprobe": 10},
}
result = hello_milvus.search(vectors_to_search, "embeddings", search_params, limit=3, output_fields=["random"])
```




# Step 4: Querying and Aggregating Data
## 1. Performs a vector query:
```angular2html
result = hello_milvus.query(expr="random > -14", output_fields=["random", "embeddings"])
```
## 2. Performs a hybrid search:
```angular2html
result = hello_milvus.search(vectors_to_search, "embeddings", search_params, limit=3, expr="random > -12", output_fields=["random"])
```

## 3. Deletes entities by their primary keys:
```angular2html
expr = f"pk in [{entities[0][0]}, {entities[0][1]}]"
hello_milvus.delete(expr)
```
## 4. Drops the collection:
```angular2html
utility.drop_collection("hello_milvus")
```

## 5. Run Milvus using Python
Execute the following command to run the example code.
```angular2html
python3 hello_milvus.py
```


# Step 5: Visualizing the Output
The returned results and query latency are shown as follows:
```angular2html
(base) supergeorge@wangzhengqideMacBook-Pro MilvusProject % python3 hello_milvus.py

=== start connecting to Milvus     ===

Does collection hello_milvus exist in Milvus: False

=== Create collection `hello_milvus` ===


=== Start inserting entities       ===

Number of entities in Milvus: 3000

=== Start Creating index IVF_FLAT  ===


=== Start loading                  ===


=== Start searching based on vector similarity ===

hit: id: 2998, distance: 0.0, entity: {'random': 0.9728033590489911}, random field: 0.9728033590489911
hit: id: 1262, distance: 0.08883658051490784, entity: {'random': 0.2978858685751561}, random field: 0.2978858685751561
hit: id: 1265, distance: 0.09590047597885132, entity: {'random': 0.3042039939240304}, random field: 0.3042039939240304
hit: id: 2999, distance: 0.0, entity: {'random': 0.02316334456872482}, random field: 0.02316334456872482
hit: id: 1580, distance: 0.05628090724349022, entity: {'random': 0.3855988746044062}, random field: 0.3855988746044062
hit: id: 2377, distance: 0.08096685260534286, entity: {'random': 0.8745922204004368}, random field: 0.8745922204004368
search latency = 0.3148s

=== Start querying with `random > 0.5` ===

query result:
-{'random': 0.6378742006852851, 'embeddings': [0.20963514, 0.39746657, 0.12019053, 0.6947492, 0.9535575, 0.5454552, 0.82360446, 0.21096309], 'pk': '0'}
search latency = 0.4312s
query pagination(limit=4):
        [{'pk': '0', 'random': 0.6378742006852851}, {'pk': '100', 'random': 0.5763523024650556}, {'pk': '1000', 'random': 0.9425935891639464}, {'pk': '1001', 'random': 0.7893211256191387}]
query pagination(offset=1, limit=3):
        [{'random': 0.5763523024650556, 'pk': '100'}, {'random': 0.9425935891639464, 'pk': '1000'}, {'random': 0.7893211256191387, 'pk': '1001'}]

=== Start hybrid searching with `random > 0.5` ===

hit: id: 2998, distance: 0.0, entity: {'random': 0.9728033590489911}, random field: 0.9728033590489911
hit: id: 747, distance: 0.14606499671936035, entity: {'random': 0.5648774800635661}, random field: 0.5648774800635661
hit: id: 2527, distance: 0.1530652642250061, entity: {'random': 0.8928974315571507}, random field: 0.8928974315571507
hit: id: 2377, distance: 0.08096685260534286, entity: {'random': 0.8745922204004368}, random field: 0.8745922204004368
hit: id: 2034, distance: 0.20354536175727844, entity: {'random': 0.5526117606328499}, random field: 0.5526117606328499
hit: id: 958, distance: 0.21908017992973328, entity: {'random': 0.6647383716417955}, random field: 0.6647383716417955
search latency = 0.2025s

=== Start deleting with expr `pk in ["0" , "1"]` ===

query before delete by expr=`pk in ["0" , "1"]` -> result: 
-{'random': 0.6378742006852851, 'embeddings': [0.20963514, 0.39746657, 0.12019053, 0.6947492, 0.9535575, 0.5454552, 0.82360446, 0.21096309], 'pk': '0'}
-{'random': 0.43925103574669633, 'embeddings': [0.52323616, 0.8035404, 0.77824664, 0.80369574, 0.4914803, 0.8265614, 0.6145269, 0.80234545], 'pk': '1'}

query after delete by expr=`pk in ["0" , "1"]` -> result: []


=== Drop collection `hello_milvus` ===
```

We have started Milvus standalone and performed first vector similarity search.

We can also check in the Docker to see it is correctly run. \
Docker Container:
![Docker container](./Images/Docker_Container.png)
Docker Image:
![Docker image](./Images/Docker_Image.png)













